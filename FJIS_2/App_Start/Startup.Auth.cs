﻿using System;
using System.IO;
using FJIS2_2.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Interop;
using Owin;

namespace FJIS2_2
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie

            //    CookieAuthenticationProvider provider = new CookieAuthenticationProvider();

            //   var originalHandler = provider.OnApplyRedirect;

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Login/Redirect"),
                //CookieDomain = ".tncourts.gov",
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromMinutes(360),
                CookieName = ".AspNetCore.Cookies",

                TicketDataFormat = new AspNetTicketDataFormat(
                        new DataProtectorShim(
                            //DataProtectionProvider.Create(new DirectoryInfo(@"C:\Projects\shared-auth-ticket-keys-testsite\"))   
                            //  **** PROD    F:\apps\shared-auth-ticket-keys\
                            DataProtectionProvider.Create(new DirectoryInfo(@"F:\WebApps\shared-auth-ticket-keys-testsite\"))
                            .CreateProtector("Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationMiddleware",
                            "Cookies", "v2"))
                ),

                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(360),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            //var cao = new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
            //    LoginPath = new PathString("/Account/Login"),
            //    Provider = new CookieAuthenticationProvider { OnApplyRedirect = ApplyRedirect }
            //};

            //app.UseCookieAuthentication(cao);

            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,                
            //    //LoginPath = new PathString("/SSOLogin"),
            //    LoginPath = new PathString("/Account/Login"),
            //    Provider = new CookieAuthenticationProvider
            //    {
            //        // Enables the application to validate the security stamp when the user logs in.
            //        // This is a security feature which is used when you change a password or add an external login to your account.  
            //        OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
            //            validateInterval: TimeSpan.FromMinutes(30),
            //            regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager)),
            //        //OnApplyRedirect = context =>
            //        //{
            //        //    //insert your logic here to generate the redirection URI
            //        //    string NewURI = "http://10.170.102.37/SSODev/Account/Login?returnUrl=Rule12";
            //        //    //Overwrite the redirection uri
            //        //    context.RedirectUri = NewURI;
            //        //    originalHandler.Invoke(context);
            //        //}
            //    }
            //});            

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
        //private static void ApplyRedirect(CookieApplyRedirectContext context)
        //{
        //    //UrlHelper _url = new UrlHelper(HttpContext.Current.Request.RequestContext);
        //    String actionUri = "http://10.170.102.37/SSODev/Account/Login?returnUrl=Rule12";  // _url.Action("Login", "Account", new { });
        //    context.Response.Redirect(actionUri);
        //}
    }
}