using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using Microsoft.AspNet.Identity;
using FJIS2_2.Models;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using Microsoft.Owin.Security;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using FJIS2_2_Data;

namespace FJIS2_2
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IDbConnection, DbConnection>();
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(o => HttpContext.Current.GetOwinContext().Authentication));
            container.RegisterType<ApplicationSignInManager>();
            container.RegisterType<ApplicationUserManager>();

            //container.RegisterType<ISqlDapperRepository, SqlDapperRepository>();
            //container.RegisterType(typeof(ISqlGenericClassRepository<>), typeof(SqlGenericClassRepository<>));
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}