﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FJIS2_2
{
    public static class Constants
    {
        public static class DefaultValues
        {
            //add default values here
            public static readonly string SSOLoginPath = ConfigurationManager.AppSettings["SSOLoginPath"].ToString();
        }
                
        public static class EmailPaths
        {
            public const string GeneralImagePath = "~/Content/images/justice_black.png";
            public const string ErrorImagePath = "~/Content/images/oops.gif";
            public const string EmailSignaturePath = "~/App_Data/emails/emailSignature.html";
            public const string ConfidentialityNoticePath = "~/App_Data/emails/confidentialityNotice.html";

            public const string SampleEmailPath = "~/App_Data/emails/Sample.html";
            public const string ErrorEmailPath = "~/App_Data/emails/Error.html";
        }

        public static class PrimaryKeys
        {
            //add primary key values here
        }
        
        public static class StoredProcedures
        {
            //add stored procedure names here
        }

        public static class OracleProcedures
        {
            //add oracle procedure names here
        }

        public static class Messages
        {
            //add system messages here
        }
    }
}