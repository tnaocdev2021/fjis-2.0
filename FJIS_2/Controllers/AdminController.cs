﻿using FJIS2_2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FJIS2_2_Domain.Models;
using FJIS_2.Helpers;
using FJIS2_2_Data;
using Dapper;
using FJIS_2_Domain.Models;

namespace FJIS_2.Controllers
{

    [Authorize(Roles = "FJIS2 Admin")]
    public class AdminController : BaseController
    {
        private CommonListHelper listHelper = new CommonListHelper();
        private IDapperRepository iRepository = new DapperRepository();
        //private DynamicParameters dp;
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.CaseJudgementList = new List<CaseJudgement>();
            ViewBag.CaseDetails = new List<CaseJudgement>();
            ViewBag.ShowResults = false;

            
            return View();
        }
        

        [HttpPost]
        public ActionResult DoCaseJudgementSearch(CaseJudgement model, string[] CaseNumbers, string SubmitBtn)
        {
            model.IsValid = true;
            TempData["CaseJudgementList"] = model;
            TempData["CaseDetails"] = model;
            return RedirectToAction("Search");
        }
        public ActionResult Search()
        {
            CaseJudgement Case = TempData.Peek("CaseJudgementList") == null ? new CaseJudgement() : (CaseJudgement)TempData.Peek("CaseJudgementList");
            
            ViewBag.CaseJudgementList = listHelper.GetCaseJudgementBySearch(Case);
            return View();
        }
       
        
        [HttpPost]
        public ActionResult ActivityLog(string CaseNumbers, string SubmitBtn, int[] Agencies)
        {
            if (CaseNumbers != null)
            {
                switch (SubmitBtn)
                {
                    case "ActivityLog":
                        ViewBag.ActivityLog = listHelper.GetActivityLog(CaseNumbers).ToList();
                        ViewBag.Details = listHelper.GetCaseDetails(CaseNumbers).ToList();
                        return View();

                    case "Redistribution":
                        List<Redistribution> RedistributionModel = new List<Redistribution>();
                        
                            foreach (var Agency in Agencies)
                            {
                                listHelper.SaveRedistribution(CaseNumbers, Agency);
                            }
                            ViewBag.Redistribution = listHelper.GetRedistribution(CaseNumbers).ToList();
                        
                        return View("Redistribution"); 
                    default:
                        break;
                }
            }
            else
            {
                ViewBag.Message = "No record selected";
            }
            return View("Index");
        }
    }
}