﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Web.Mvc;
using Dapper;

namespace FJIS2_2
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext exception)
        {
            if (exception.ExceptionHandled)
            {
                return;
            }
            exception.Result = new ViewResult()
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(exception.Exception)
            };
            exception.ExceptionHandled = true;
        }
        
        #region Alerts
        public class Alert
        {
            public const string TempDataKey = "TempDataAlerts";
            public string AlertStyle { get; set; }
            public string AlertIcon { get; set; }
            public string Message { get; set; }
            public bool Dismissable { get; set; }
            public static class AlertStyles
            {
                public const string Success = "success";
                public const string Information = "info";
                public const string Warning = "warning";
                public const string Danger = "danger";
            }

            public static class AlertIcons
            {
                public const string Success = "check";
                public const string Information = "info";
                public const string Warning = "warning";
                public const string Danger = "exclamation-circle";
            }
        }
        public void Success(string message, bool dismissable = true)
        {
            AddAlert(Alert.AlertStyles.Success, Alert.AlertIcons.Success, message, dismissable);
        }
        public void Information(string message, bool dismissable = true)
        {
            AddAlert(Alert.AlertStyles.Information, Alert.AlertIcons.Information, message, dismissable);
        }
        public void Warning(string message, bool dismissable = true)
        {
            AddAlert(Alert.AlertStyles.Warning, Alert.AlertIcons.Warning, message, dismissable);
        }
        public void Danger(string message, bool dismissable = true)
        {
            AddAlert(Alert.AlertStyles.Danger, Alert.AlertIcons.Danger, message, dismissable);
        }
        private void AddAlert(string alertStyle, string alertIcon, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                AlertIcon = alertIcon,
                Message = message,
                Dismissable = dismissable
            });

            TempData[Alert.TempDataKey] = alerts;
        }
        #endregion

        #region Emails
        public void SendEmail(string toAddress, string subject, string emailPath, string[] args, bool useHtml, string fromAddress = "")
        {            
            using (SmtpClient client = new SmtpClient())
            {                
                MailMessage message = new MailMessage();
                message.To.Add(toAddress);
                message.Subject = subject;
                message.IsBodyHtml = useHtml;
                message.AlternateViews.Add(GetAlternateView(emailPath,args));
                client.Send(message);
            }
        }

        private AlternateView GetAlternateView(string emailPath, string[] args)
        {
            string body = System.IO.File.ReadAllText(Server.MapPath(emailPath)), imgPath = imgPath = Constants.EmailPaths.GeneralImagePath;
            string signature = System.IO.File.ReadAllText(Server.MapPath(Constants.EmailPaths.EmailSignaturePath));
            string notice = System.IO.File.ReadAllText(Server.MapPath(Constants.EmailPaths.ConfidentialityNoticePath));
            switch (emailPath)
            {
                case Constants.EmailPaths.ErrorEmailPath:
                    imgPath = Constants.EmailPaths.ErrorImagePath;
                    break;
            }

            if (args != null && args.Length > 0)
            {
                body = String.Format(body, args);
            }

            body = body.Replace("Insert_Signature", signature);
            body = body.Replace("Insert_ConfidentialityNotice", notice);

            string headerPic = System.Web.HttpContext.Current.Server.MapPath(imgPath);
            var aView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            LinkedResource res = new LinkedResource(headerPic);
            res.ContentId = "headerPic";
            aView.LinkedResources.Add(res);
            return aView;
        }

        private string[] SetErrorArgs(Exception ex)
        {
            
            string[] args =
            {
                Assembly.GetExecutingAssembly().GetName().Name,
                DateTime.UtcNow.ToLocalTime().ToString(),
                ex.Message,
                (ex.InnerException == null) ? "N/A" : ex.InnerException.ToString(),
                ex.StackTrace,
                "User Name", // change to get logged in users name
                "User Email", // change to get logged in users email
                "User Phone Number" // change to get logged in users phone number
            };
            return args;
        }
        #endregion       
        
    }
}