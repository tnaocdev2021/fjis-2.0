﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FJIS2_2
{
    public class ErrorController : BaseController
    {
        public ActionResult Error(Exception ex)
        {
            return View(ex);
        }

        public ActionResult PageNotFound()
        {
            return View();
        }

        public ActionResult InternalServerError()
        {
            return View();
        }
    }
}