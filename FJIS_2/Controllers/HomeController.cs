﻿using System.Web.Mvc;

namespace FJIS2_2.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController()
        {
        }
        [Authorize]
        public ActionResult Acknowledge()
        {
            return View();
        }
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            Session["isAuthenticated"] = true;
            // I think this is enough to keep a session active if an AJAX request posts here.
            return new JsonResult { Data = "Success" };
        }
    }
}