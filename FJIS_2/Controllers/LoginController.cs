﻿using FJIS2_2.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FJIS2_2.Areas.Identity.Controllers
{
    public class LoginController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Redirect", "Login");
        }

        

        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //   return RedirectToAction("Redirect", "Login");
        //}

        #region helpers       
        [AllowAnonymous]
        public void Redirect()
        {
            Response.Redirect(Constants.DefaultValues.SSOLoginPath);
        }
        #endregion
    }
}