﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FJIS2_2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            HttpException httpEx = ex as HttpException;
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            if (httpEx == null)
            {
                routeData.Values.Add("action", "Error");
                routeData.Values.Add("ex", ex);
            }
            else
            {
                switch (httpEx.GetHttpCode())
                {
                    case 404:
                        routeData.Values.Add("action", "PageNotFound");
                        break;
                    case 500:
                        routeData.Values.Add("action", "InternalServerError");
                        break;
                    default:
                        //throw ex;
                        routeData.Values.Add("action", "Error");
                        routeData.Values.Add("ex", ex);
                        break;
                }
            }
            Server.ClearError();
            IController errorController = new ErrorController();
            Response.ContentType = "text/html";
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}
