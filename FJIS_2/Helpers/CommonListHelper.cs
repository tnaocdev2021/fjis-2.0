﻿using Dapper;
using FJIS2_2_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FJIS2_2_Domain.Models;
using FJIS_2_Domain.Models;
using Microsoft.AspNet.Identity;
using System.Configuration;

namespace FJIS_2.Helpers
{
    public class CommonListHelper
    {
        
        private IDapperRepository iRepository = new DapperRepository();

        public CommonListHelper() { }
        public CommonListHelper(IDapperRepository repository)
        {
            iRepository = repository;
        }
        public List<CaseJudgement> GetCaseJudgementBySearch(CaseJudgement model)
        {
             DynamicParameters dp = new DynamicParameters();
           
            if (model.DateReceived2 != null)
            {
                dp.Add("ReciveDate2", model.DateReceived2);
            }
            else
            {
                dp.Add("ReciveDate2", model.DateReceived1);
            }

            dp.Add("JudicialDistrict", model.JudicialDistrict);
            dp.Add("JudicialCounty", model.JudicialCounty);
            dp.Add("ReciveDate1", model.DateReceived1);
            //dp.Add("ReciveDate2", model.DateReceived2);

            IGenericClassRepository<CaseJudgement> repository = new GenericClassRepository<CaseJudgement>();
            List<CaseJudgement> CaseJudgementData = repository.GetListByStoredProcedure("dbo.SearchCaseJudgement", dp).ToList();

            return CaseJudgementData;
        }
        public List<ActivityLog> GetCaseDetails(string CaseNumber)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("CaseNumber", CaseNumber);

            IGenericClassRepository<ActivityLog> repository = new GenericClassRepository<ActivityLog>();
            List<ActivityLog> CaseDetailData = repository.GetListByStoredProcedure("dbo.GetDetails", dp).ToList();

            return CaseDetailData;
        }

        public List<ActivityLog> GetActivityLog(string CaseNumber)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("CaseNumber", CaseNumber);

            IGenericClassRepository<ActivityLog> repository = new GenericClassRepository<ActivityLog>();
            List<ActivityLog> CaseActivityLog = repository.GetListByStoredProcedure("dbo.GetActivityLog", dp).ToList();

            return CaseActivityLog;
        }
        public List<Redistribution> GetRedistribution(string CaseNumber)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("CaseNumber", CaseNumber);

            IGenericClassRepository<Redistribution> repository = new GenericClassRepository<Redistribution>();
            List<Redistribution> CaseActivityLog = repository.GetListByStoredProcedure("dbo.GetRedistribution", dp).ToList();

            return CaseActivityLog;
        }

        public void SaveRedistribution(string caseNumber, int Agency)
        {
            var user = System.Threading.Thread.CurrentPrincipal;
            string EventUserID = user.Identity.GetUserName();
            DynamicParameters dp = new DynamicParameters();

            //dp.Add("UniqueJudgmentID", model.UniqueJudgmentID);
            dp.Add("CaseNumber", caseNumber);
            dp.Add("EventID", Agency);
            dp.Add("EventStatus", "Success");
            if (Agency == 6)
            {
                dp.Add("EventMessage", "Success: Redistributed to TBI");
            }
            else
            {
                dp.Add("EventMessage", "Success: Redistributed to TDOC");
            }
            
            dp.Add("EventDate", DateTime.Now);
            if (EventUserID == "")
            {
                dp.Add("EventUserID", "System");
            }
            else
            {
                dp.Add("EventUserID", EventUserID);
            }
            

            iRepository.ExecuteByStoredProcedureNoReturn("dbo.InsertRedistribution", dp);
        }
    }
}