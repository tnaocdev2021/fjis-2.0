﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Principal;

namespace FJIS2_2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //public string LastName { get; set; }
        //public string FirstName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            //userIdentity.AddClaim(new Claim("DisplayName", this.Email));  // FirstName.ToString() + " " + LastName.ToString()));
            //userIdentity.AddClaim(new Claim("UserRole", manager.GetRoles(manager.FindByName(userIdentity.Name).Id)[0]));


            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            //var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("AMP_Conn", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public static class IdentityExtensions
    {
        public static string GetFullName(this IIdentity user)
        {
            if (user.IsAuthenticated)
            {
                var claim = ((ClaimsIdentity)user).FindFirst("DisplayName");
                return (claim != null) ? claim.Value : "";
            }
            else
            {
                return "";
            }
        }

        public static string GetRole(this IIdentity user)
        {
            if (user.IsAuthenticated)
            {
                var claim = ((ClaimsIdentity)user).FindFirst("Role1");
                return (claim != null) ? claim.Value : "";
            }
            else
            {
                return "";
            }
        }
    }
}