﻿$(document).ready(function () {

    // 1st replace first column header text with checkbox

    //$("#WebGrid th").each(function () {
    //    if ($.trim($(this).text().toString().toLowerCase()) === "{checkall}") {
    //        $(this).text('');
    //        $("<input/>", { type: "checkbox", id: "cbSelectAll", value: "" }).appendTo($(this));
    //        $(this).append("<span>Select All</span>");
    //    }
    //});

    //2nd click event for header checkbox for select /deselect all
    $("#cbSelectAll").live("click", function () {
        var ischecked = this.checked;
        $('#WebGrid').find("input:checkbox").each(function () {
            this.checked = ischecked;
        });
    });
    $('.select').click(function () {
        $('.select').not(this).prop('checked', false);
    });

    //3rd click event for checkbox of each row
    $("input[name='CaseNumbers']").click(function () {
        var totalRows = $("#WebGrid td :checkbox").length;
        var checked = $("#WebGrid td :checkbox:checked").length;

        if (checked == totalRows) {
            $("#WebGrid").find("input:radio").each(function () {
                this.checked = true;
            });
        }
        else {
            $("#cbSelectAll").removeAttr("checked");
        }
    });

});