﻿function HideSessionReset() {
    $("#btnKeepSession").hide();
    $("#lblSession").addClass("session");
    $("#lblSession").removeClass("sessionEnding");
    $("#lblSession").text('');
}

var updateSession1 = function () {
    if (sessionTimeout1 > 0) {
        sessionTimeout1 -= 1;
    }
    var display = "";
    if (sessionTimeout1 >= 60) {
        var min = parseInt(sessionTimeout1 / 60);
        var sec = sessionTimeout1 - min * 60;
        if (sec == 0) {
            display = "Session timeout: " + min.toString() + "m";
        } else {
            display = "Session timeout: " + min.toString() + "m " + sec.toString() + "s";
        }
    }
    else {
        display = "Session timeout: " + sessionTimeout1 + "s"
    }
    if (sessionTimeout1 <= 59) {
        $("#btnKeepSession").show();
        $("#lblSession").removeClass("session");
        $("#lblSession").addClass("sessionEnding");
    }

    if (sessionTimeout1 == 0) {
        $("#lblSession").text('Session: ');
        $("#lblSession").text('expired!');
        $("#btnKeepSession").hide();
        display = "Session: expired!";
    }
    $("#lblSession").text(display);
};