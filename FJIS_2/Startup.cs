﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FJIS2_2.Startup))]
namespace FJIS2_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
