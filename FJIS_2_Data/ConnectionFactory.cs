﻿using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Data.SqlClient;

namespace FJIS2_2_Data
{
    public class ConnectionFactory
    {
        public SqlConnection GetDefaultConnection()
        {
            string defaultConnectionString = ConfigurationManager.ConnectionStrings["TNACJConnection"].ConnectionString;
            return new SqlConnection(defaultConnectionString);
        }
    }
}
