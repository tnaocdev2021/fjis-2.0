﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS2_2_Data
{
    public class DapperRepository : IDapperRepository
    {
        private ConnectionFactory connectionFactory;
        public DapperRepository()
        {
            connectionFactory = new ConnectionFactory();
        }

        public bool ExecuteByStoredProcedureReturnBool(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                bool result = (bool)conn.ExecuteScalar(spName, dp, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public int ExecuteByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                string result = conn.ExecuteScalar(spName, dp, commandType: CommandType.StoredProcedure).ToString();
                return Int32.Parse(result);
            }
        }

        public int ExecuteByQuery(string query, int id)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                query += " '" + id + "'";
                return conn.Query<int>(query, null).SingleOrDefault();
            }
        }

        public void ExecuteByStoredProcedureNoReturn(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                conn.Query(spName, dp, commandType: CommandType.StoredProcedure).ToString();
            }
        }

        public IQueryable<dynamic> GetListByQuery(string query)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<dynamic>(query).AsQueryable();
            }
        }

        public IQueryable<dynamic> GetListByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<dynamic>(spName, dp, commandType: CommandType.StoredProcedure).AsQueryable();
            }
        }

        public dynamic GetSingleRecordByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<dynamic>(spName, dp, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public dynamic GetSingleRecordByQuery(string query, int id)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<dynamic>(query, new { Id = id }).SingleOrDefault();
            }
        }
    }
}