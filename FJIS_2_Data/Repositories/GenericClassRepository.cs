﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FJIS2_2_Data
{
    public class GenericClassRepository<T> : IGenericClassRepository<T> where T : class
    {        
        private ConnectionFactory connectionFactory;
        public GenericClassRepository()
        {
            connectionFactory = new ConnectionFactory();
        }

        public IQueryable<T> GetListByQuery(string query)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<T>(query).AsQueryable();
            }
        }
        
        public IQueryable<T> GetListByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<T>(spName, dp, commandType: CommandType.StoredProcedure).AsQueryable();
            }
        }
                
        public T GetSingleRecordByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<T>(spName, dp, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public T GetSingleRecordByQuery(string query, int id)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Query<T>(query, new { Id = id }).SingleOrDefault();
            }
        }

        public int ExecuteByStoredProcedure(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                return conn.Execute(spName, dp, commandType: CommandType.StoredProcedure);
            }
        }

        public int ExecuteByQuery(string query, int id)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                query += " '" + id + "'";
                return conn.Query<int>(query, null).SingleOrDefault();
            }
        }

        public int ExecuteByStoredProcedureScalar(string spName, DynamicParameters dp)
        {
            using (var conn = connectionFactory.GetDefaultConnection())
            {
                string result = conn.ExecuteScalar(spName, dp, commandType: CommandType.StoredProcedure).ToString();
                return Int32.Parse(result);
            }
        }
    }
}
