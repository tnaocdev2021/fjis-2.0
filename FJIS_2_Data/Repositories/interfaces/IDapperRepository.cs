﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS2_2_Data
{
    public interface IDapperRepository
    {
        int ExecuteByStoredProcedure(string spName, DynamicParameters dp);
        int ExecuteByQuery(string query, int id);

        void ExecuteByStoredProcedureNoReturn(string spName, DynamicParameters dp);

        IQueryable<dynamic> GetListByStoredProcedure(string spName, DynamicParameters dp);
        IQueryable<dynamic> GetListByQuery(string query);

        dynamic GetSingleRecordByStoredProcedure(string spName, DynamicParameters dp);
        dynamic GetSingleRecordByQuery(string query, int id);

        bool ExecuteByStoredProcedureReturnBool(string spName, DynamicParameters dp);
    }
}
