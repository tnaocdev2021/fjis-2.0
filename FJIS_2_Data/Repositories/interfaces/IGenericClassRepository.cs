﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace FJIS2_2_Data
{
    public interface IGenericClassRepository<T> where T : class
    {
        IQueryable<T> GetListByQuery(string query);
        IQueryable<T> GetListByStoredProcedure(string spName, DynamicParameters dp);
        T GetSingleRecordByQuery(string query, int id);
        T GetSingleRecordByStoredProcedure(string spName, DynamicParameters dp);
        int ExecuteByStoredProcedure(string spName, DynamicParameters dp);
        int ExecuteByQuery(string query, int id);

        int ExecuteByStoredProcedureScalar(string spName, DynamicParameters dp);
    }
}
