﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS2_2_Domain.Models
{
    public class ActivityLog
    {
        public string UniqueJudgmentID { get; set; }
        public string EventID { get; set; }
        public string JudicialDistrict { get; set; }
        public string JudicialCounty { get; set; }
        public string CaseNumber { get; set; }
        public string CountNumber { get; set; }
        public DateTime? EventDate  { get; set; }
        public string EventUserID { get; set; }
        public string EventStatus { get; set; }
        public string EventMessage { get; set; }
        public string EventName { get; set; }



        public string JudicialDivision { get; set; }
        public string JudgmentTypeCode { get; set; }
        public DateTime? JudgmentEntryDateValue { get; set; }
        //public List<ActivityLog> activityLogs { get; set; }
    }
}
