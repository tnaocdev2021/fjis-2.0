﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS_2_Domain.Models
{
    public class CaseDetails
    {
        public string JudicialDivision { get; set; }
        public string JudgmentTypeCode { get; set; }
        public string CountNumber { get; set; }
        public string CaseNumber { get; set; }
        public string JudgmentEntryDateValue { get; set; }
        public bool ActivityLog { get; set; }
    }
}
