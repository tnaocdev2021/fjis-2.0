﻿using FJIS_2_Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS2_2_Domain.Models
{
    public class CaseJudgement
    {
        public string UniqueJudgmentID { get; set; }
        public string JudicialDistrict { get; set; }
        public string JudicialCounty { get; set; }
        public string CaseNumber { get; set; }
        public DateTime? JudgmentEntryDateValue { get; set; }
        public DateTime? DateReceived1 { get; set; }
        public DateTime? DateReceived2 { get; set; }
        public DateTime? Created_Date { get; set; }
        public DateTime? DateDistributed { get; set; }
        public string CountNumber { get; set; }
        public bool IsValid { get; set; }
        public bool Select { get; set; }
        public List<CaseDetails> caseDetails { get; set; }
        //public List<ActivityLog> activityLogs { get; set; }
        public List<Redistribution> redistribution { get; set; }
       


    }
}
