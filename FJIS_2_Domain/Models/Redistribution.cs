﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FJIS2_2_Domain.Models
{
    public class Redistribution
    {
        public string JudicialDistrict  { get; set; }
        public string JudicialCounty { get; set; }
        public string CaseNumber { get; set; }
        public int          EventID			    { get; set; }
        public string       EventName             { get; set; }
        public DateTime?    EventDate			{ get; set; }
        public string       EventUserID		    { get; set; }
        public string       EventMessage		    { get; set; }
        //public string EventStatus		 { get; set; }
    }
}
